## Day 12 
1. `npm install --save express`
1. `npm install --save mysql`
1. `npm install --save path`
1. `npm install uuid --save`
1. Create `schema.sql`
1. `CREATE {DATABASE | SCHEMA} [IF NOT EXISTS] db_name`
1. `INSERT INTO tbl_name () VALUES();`
1. at server/main.js `app.use("/libs", express.static(path.join(__dirname,"../bower_components")));`
1. at index.html `<link rel="stylesheet" href="/libs/bootstrap/dist/css/bootstrap.css">`
1. if `.gitignore` not working, `git rm -r --cached . ` `git add .` `git commit -m "fixed untracked files"  `