create database if not exists mydb;
use mydb;

create table registrations 
(  
    regid char(8) not null,
    email varchar(256) not null,     
    name varchar(256) not null,     
    phone varchar(32) not null,     
    dob date not null,
    primary key(regid)  
);
