//load libs
const express = require("express");
const path = require("path");
const mysql = require("mysql");
const uuid = require('uuid');

//create an instance of express
const app = express();

app.use(express.static(path.join(__dirname, "../client")));
app.use("/libs", express.static(path.join(__dirname,"../bower_components")));

//Configure mysql pool
const pool = mysql.createPool({
    host: "localhost", port: 3306, //Server
    user: "user", password: "user", //login
    database: "mydb",
    connectionLimit: 4
});
// Error handler
const handleError = function(err, resp) {
    resp.status(500);
    resp.type("text/plain");
    resp.send(JSON.stringify(err));
};

const SQL_SELECT_REGISTRATIONS = "select count(*) as email_exist from registrations where email = ?";
//INSERT INTO tbl_name () VALUES();
const SQL_INSERT_REGISTRATIONS = 
"insert into registrations (regid, username, email, phone, dob) values (?,?,?,?,?)";

const generateId = function (){
    var myId = uuid().substring(0,8);
    
    console.info(">>>myId >>",myId );
    return myId;
    
};

app.get("/register", function(req,resp){
    //Get a connection from the pool
    //Error or a connection object
    pool.getConnection(function(err, conn) {
        //Check if the pool has returned an error
        if (err) {
            console.error("Error: ", err);
            handleError(err, resp);
            return;
        }
        var msg = "";
        var userEmail = req.query.email.toLowerCase();
        var userName = req.query.username;
        var userPhone = req.query.phone;
        var userDob = req.query.dob;

        //console.info (">>> userEmail >> ", userEmail);
        //console.info(">>>SQL query>>>" , SQL_SELECT_REGISTRATIONS);
        conn.query(SQL_SELECT_REGISTRATIONS, [userEmail], function(err, result) {
            //Check if the query has resulted in an error
            if (err) {
                console.error(">>>> select: ", err);
                resp.status(500);
                resp.end(JSON.stringify(err));

                conn.release();
                return;
            }
                //console.info("email exist >>>", result[0].email_exist);
                //Email not exist in the database
                if (result[0].email_exist == 0){
                    // call function to get regId
                    var regId = generateId();
                    // Insert user information into database
                    conn.query(SQL_INSERT_REGISTRATIONS
                        , [ regId, userName, userEmail, userPhone, userDob ]
                        , function(err, result) {
                            if (err) {
                                console.error(">>>> insert: ", err);
                                resp.status(500);
                                resp.end(JSON.stringify(err));
                                //conn.release();
                                return;
                            }
                            //INSERT successfully, return msg
                            msg = 
                                userName + ", Thank you for your registration! <br/> Your registration Id: " + regId;
                            resp.status(200);
                            resp.type("text/html");
                            resp.send("<h1> " + msg + "</h1>");
                        });

                } else {
                    // email_exist <> 0, Email already exist, return msg
                    msg = "Email already exist! Please use different email address.";
                    resp.status(200);
                    resp.type("text/html");
                    resp.send("<h1> " + msg + "</h1>");
                }
                //release connection
                conn.release();
            });
            
        });
    });




//configure the port
const port = process.env.APP_PORT || 3000;

app.listen(port, function() {
    console.info("Application start at %s on port %d", new Date(), port);
});